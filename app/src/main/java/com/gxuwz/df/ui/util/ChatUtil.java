package com.gxuwz.df.ui.util;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import androidx.annotation.NonNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class ChatUtil {

    private static ChatUtil instance = null;

    private Socket socket;
    private BufferedReader reader;
    private OutputStream output;
    private ThreadPoolExecutor poolExecutor;
    private Handler sendHandler;
    private Handler retHandler;

    public static ChatUtil getInstance(String ip, int port,Handler retHandler) {
        if (instance == null) {
            synchronized (ChatUtil.class) {
                if (instance == null) {
                    instance = new ChatUtil(ip, port,retHandler);
                }
            }
        }
        return instance;
    }

    public ChatUtil(final String ip, final int port, final Handler retHandler) {
        this.retHandler = retHandler;
        poolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
        poolExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    socket = new Socket(ip, port);
                    reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    output = socket.getOutputStream();

                    poolExecutor.execute(new Runnable() {
                        @Override
                        public void run() {
                            String mess = null;
                            try {
                                while (((mess = reader.readLine()) != null)) {
                                    Message msg = new Message();
                                    msg.what = 1;
                                    msg.obj = mess;
                                    retHandler.sendMessage(msg);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    Looper.prepare();
                    sendHandler = new Handler() {
                        @Override
                        public void handleMessage(@NonNull Message msg) {
                            super.handleMessage(msg);
                            try {
                                if (msg.what == 1) {
                                    output.write((msg.obj.toString() + "\n").getBytes(StandardCharsets.UTF_8));
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }
                    };
                    Looper.loop();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    Message message;
    public void sendMess(String mess) {
        if(sendHandler != null){
            message = new Message();
            message.what = 1;
            message.obj = mess;
            sendHandler.sendMessage(message);
        }
    }

    public void stopChat() {
        poolExecutor.shutdownNow();
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
