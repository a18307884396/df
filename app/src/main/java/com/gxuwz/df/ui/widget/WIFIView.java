package com.gxuwz.df.ui.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

public class WIFIView extends View {

    private int width;
    private int height;
    private Paint paint;

    public WIFIView(Context context) {
        super(context);
        init();
    }

    public WIFIView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public WIFIView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public WIFIView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init(){
        paint = new Paint();
        //画笔颜色
        paint.setColor(Color.BLACK);
        //画笔粗细
        paint.setStrokeWidth(8);
        paint.setStyle(Paint.Style.STROKE);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        if(widthMode == MeasureSpec.EXACTLY){
            width = widthSize;
        }
        if(heightMode == MeasureSpec.EXACTLY){
            height = heightSize;
        }
        setMeasuredDimension(width,height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int left = 0;
        int top = 0;
        int right = width;
        int bottom = height;
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawArc(left,top,right,bottom,
                225,90,false,paint);
        for(int i = 0;i < 2;i++){
            left += 3 * 10;
            top += 3 * 10;
            right -= 3 * 10;
            bottom -= 3 * 10;
            canvas.drawArc(left,top,right,bottom,
                    225,90,false,paint);
        }
        paint.setStyle(Paint.Style.FILL);
        left += 3 * 10;
        top += 3 * 10;
        right -= 3 * 10;
        bottom -= 3 * 10;
        canvas.drawArc(left,top,right,bottom,
                225,90,true,paint);

        super.onDraw(canvas);
    }
}
