package com.gxuwz.df.ui.fragment.viewconponent;

import android.app.Activity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import com.gxuwz.df.R;
import com.gxuwz.df.business.model.entity.User;
import com.gxuwz.df.ioc.DfResource;
import com.gxuwz.df.ioc.IOC;
import com.gxuwz.df.ioc.inject.ClassViewInject;
import com.gxuwz.df.ui.widget.TitleListView;
import com.gxuwz.df.ui.widget.UserFunView;
import com.gxuwz.df.ui.widget.UserSimpleMessView;

import org.greenrobot.eventbus.EventBus;

public class MyViewConponent implements View.OnClickListener {
    @DfResource(R.id.user_simple_view)
    public UserSimpleMessView userSimpleMessView;
    @DfResource(R.id.user_fun_view)
    public UserFunView userFunView;
    @DfResource(R.id.title_list_view)
    public TitleListView titleListView;
    @DfResource(R.id.anim_img)
    public ImageView animImg;

    public MyViewConponent(Activity activity) {
        ClassViewInject.inject(activity,this);
        animImg.setOnClickListener(this);
    }

    public void animTest(){
        AnimationSet set = new AnimationSet(true);

        //旋转
        Animation rotate = new RotateAnimation(0,360, Animation.RELATIVE_TO_SELF,0.5f, Animation.RELATIVE_TO_SELF,0.5f);
        rotate.setDuration(5000);
        rotate.setRepeatMode(Animation.RESTART);
        rotate.setRepeatCount(Animation.INFINITE);
        //set.addAnimation(rotate);

        //平移
        /*
        主要有两个构造函数
        1.TranslateAnimation(fromXDelta,toXDelta,fromYDelta,toYDelta)
        意思是：(x+fromXDelta,y+fromYDelta)移动到(x+toXDelta,y+toYDelta)

        2.TranslateAnimation(
         */
        Animation translate = new TranslateAnimation(Animation.RELATIVE_TO_SELF,0,
                Animation.RELATIVE_TO_SELF,1,Animation.RELATIVE_TO_SELF,0,Animation.RELATIVE_TO_SELF,1);
        translate.setDuration(10000);
        set.addAnimation(translate);

        animImg.startAnimation(set);

        User user = (User) IOC.get("User");
        user.setUserId("001");
        user.setUserName("周日月");
        EventBus.getDefault().post(user);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.anim_img){
            animTest();
        }
    }
}
