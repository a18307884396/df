package com.gxuwz.df.ui.util.retrofit;


import com.google.gson.GsonBuilder;
import com.gxuwz.df.business.utils.date.DataUtil;
import com.gxuwz.df.ioc.DfBean;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.CallAdapter;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@DfBean
public class RetrofitUtil {

    private String baseUrl = "http://10.0.0.0:8080";

    //配置客户端
    private OkHttpClient okHttpClient;
    //RxJava解析器
    private Converter.Factory converterFy = null;
    private Retrofit retrofit = null;
    //Gson适配器
    private CallAdapter.Factory callAdapterFy = null;
    //当前server
    private RetrofitServer server;

    public RetrofitUtil(){
        okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(20,TimeUnit.SECONDS)
                .readTimeout(20,TimeUnit.SECONDS)
                .writeTimeout(20,TimeUnit.SECONDS)
                .build();

        converterFy = GsonConverterFactory.create(new GsonBuilder()
                .setDateFormat(DataUtil.SIMPLE_DATE).create());
        callAdapterFy = RxJava2CallAdapterFactory.create();

        retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addConverterFactory(converterFy)
                .addCallAdapterFactory(callAdapterFy)
                .build();
        server = retrofit.create(RetrofitServer.class);
    }

    public RetrofitServer getServer(){
        return server;
    }
}
