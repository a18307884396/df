package com.gxuwz.df.ui.activity;

import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.gxuwz.df.R;
import com.gxuwz.df.ui.util.PixelUtil;
import com.gxuwz.df.ui.widget.BottomPlayView;

public abstract class BtmPlayActivity extends BaseActivity{
    protected BottomPlayView bottomPlayView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        PixelUtil.init(this);
        super.onCreate(savedInstanceState);
    }
    /** 初始化 **/
    protected void initNav(){
        //bottomPlayView = findViewById(R.id.bottom_play_view);
        BitmapDrawable bitmapDrawable = (BitmapDrawable)getResources().getDrawable(R.drawable.img_my);
        bottomPlayView.setSongImg(bitmapDrawable.getBitmap());

        bottomPlayView.setSongName("浪漫手机");
        bottomPlayView.setSingerName("周杰伦");
    }
}
