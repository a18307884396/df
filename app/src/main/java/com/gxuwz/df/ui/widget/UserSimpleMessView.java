package com.gxuwz.df.ui.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import com.gxuwz.df.business.model.vo.UserViewVo;
import com.gxuwz.df.ui.util.DFColor;
import com.gxuwz.df.ui.util.DFPaint;
import com.gxuwz.df.ui.util.DfCanvas;
import com.gxuwz.df.ui.util.PixelUtil;
import com.gxuwz.df.ui.util.TextSize;

/***
 * 用户头像及信息
 */
public class UserSimpleMessView extends View {

    int width;
    int height;

    Canvas myCanvas = DfCanvas.cloneDf();
    Paint paint;//画笔
    RectF rectF;//背景

    UserViewVo userViewVo;

    public UserSimpleMessView(Context context) {
        super(context);
        init();
    }

    public UserSimpleMessView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public UserSimpleMessView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public UserSimpleMessView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init(){
        paint = new Paint();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        if(widthMode == MeasureSpec.EXACTLY){
            this.width = widthSize;
        }
        if(heightMode == MeasureSpec.EXACTLY){
            this.height = heightSize;
        }
        setMeasuredDimension(width,height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if(userViewVo != null){
            paint.reset();
            //绘制用户头像
            if(userViewVo.getUserPicture() != null){
                canvas.drawBitmap(userViewVo.getUserPicture(),PixelUtil.getPx(20),PixelUtil.getPx(10),paint);
            }
            //绘制用户名
            if(userViewVo.getUserName() != null){
                paint.setColor(DFColor.DF_2C.getColor());
                paint.setTextSize(TextSize.THEME_TEXT.getTextSize());
                canvas.drawText(userViewVo.getUserName(),PixelUtil.getPx(110),
                        PixelUtil.getPx(25)+DFPaint.measureBase(paint),paint);
            }
            if(userViewVo.getGrade() != null){
                //paint.setTextSize(TextSize.TEXT_SIZE_2.getTextSize());
                float h = height - PixelUtil.getPx(20);
                paint.setColor(DFColor.DF_BLUE.getColor());
                canvas.drawText("Lv."+userViewVo.getGrade(),PixelUtil.getPx(110),
                        h,paint);
            }
        }
        super.onDraw(canvas);
    }

    public void dealWith(Bitmap userPiture) {
        Matrix matrix = new Matrix();
        float midSize = PixelUtil.getPx(80);
        float scale = 0;
        if(userPiture.getWidth()>midSize||userPiture.getHeight()>midSize){
            scale = Math.min(midSize/userPiture.getWidth(),midSize/userPiture.getHeight());
        } else {
            scale = Math.max(midSize/userPiture.getWidth(),midSize/userPiture.getHeight());
        }
        matrix.setScale(scale,scale);
        this.userViewVo.setUserPicture(Bitmap.createBitmap(userPiture,0,0,
                userPiture.getWidth(), userPiture.getHeight(),matrix,false));
    }

    public void setUserViewVo(UserViewVo userViewVo) {
        this.userViewVo = userViewVo;
        dealWith(userViewVo.getUserPicture());
    }

}
