package com.gxuwz.df.ui.util;

import android.graphics.Color;

public enum DFColor {

    DF_BLUE("#4e7cb0","主题蓝色"),
    WHITE("#ffffff","白色"),
    GRAY_FRA("#EBEBEB","灰色边框"),
    DF_2C("#2c2c2c","主题字体"),
    DF_70("#707070","二级主题字体"),
    TRANSPARENT("#00ffffff","透明"),
    DF_YELLOW("#ffff00","黄色");

    private int colorValue;
    private String colorName;

    DFColor(String colorValue,String colorName){
        this.colorValue = Color.parseColor(checkColor(colorValue,"value is error this color"));
        this.colorName = colorName;
    }

    public int getColor(){
        return this.colorValue;
    }

    private String checkColor(String colorValue,String message){
        if(!colorValue.startsWith("#")){
            throw new IllegalArgumentException(message);
        }
        return colorValue;
    }
}
