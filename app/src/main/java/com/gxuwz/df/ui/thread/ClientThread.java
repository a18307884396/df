package com.gxuwz.df.ui.thread;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import androidx.annotation.NonNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

public class ClientThread implements Runnable {

    //Socket对象
    private Socket socket;
    //缓存字符输入流
    private BufferedReader bufferedReader = null;
    //字节输出流
    private OutputStream outputStream = null;

    public Handler handler;

    public Handler revHandler;

    public ClientThread(Handler handler) {
        this.handler = handler;
    }

    @Override
    public void run() {
        try {
            socket = new Socket("192.168.137.1", 30003);
            bufferedReader = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));
            outputStream = socket.getOutputStream();

            new Thread() {
                @Override
                public void run() {
                    try {
                        String content = null;
                        while ((content = bufferedReader.readLine()) != null) {
                            System.out.println(content);
                            Message msg = new Message();
                            msg.what = 0;
                            msg.obj = content;
                            handler.sendMessage(msg);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }.start();

            Looper.prepare();
            revHandler = new Handler() {
                @Override
                public void handleMessage(@NonNull Message msg) {
                    if (msg.what == 1) {
                        try {
                            outputStream.write((msg.obj.toString() + "\r\n").getBytes("utf-8"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            };
            Looper.loop();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
