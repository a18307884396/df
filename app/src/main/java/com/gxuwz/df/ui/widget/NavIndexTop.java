package com.gxuwz.df.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import com.gxuwz.df.R;
import com.gxuwz.df.ui.util.DFColor;
import com.gxuwz.df.ui.util.TextSize;
import com.gxuwz.df.ui.util.PixelUtil;

public class NavIndexTop extends View {

    private int width;
    private int height;

    String[] titles;
    Bitmap leftImgBit;
    Bitmap rightImgBit;

    Paint paint;
    RectF rectF;
    int item;

    float baseline;

    Context context;
    int imgSize;

    public NavIndexTop(Context context) {
        super(context);
    }

    public NavIndexTop(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs);
    }

    public NavIndexTop(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context,attrs);
    }

    public NavIndexTop(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context,attrs);
    }

    private void init(Context context,AttributeSet attrs){
        this.context = context;
        TypedArray typedArray = context.obtainStyledAttributes(attrs,R.styleable.NavIndexTop);
        String title1 = typedArray.getString(R.styleable.NavIndexTop_title1);
        String title2 = typedArray.getString(R.styleable.NavIndexTop_title2);
        String title3 = typedArray.getString(R.styleable.NavIndexTop_title3);
        String title4 = typedArray.getString(R.styleable.NavIndexTop_title4);
        BitmapDrawable leftImgDrawable = (BitmapDrawable) typedArray.getDrawable(R.styleable.NavIndexTop_leftImg);
        BitmapDrawable rightImgDrawable = (BitmapDrawable) typedArray.getDrawable(R.styleable.NavIndexTop_rightImg);
        titles = new String[]{title1,title2,title3,title4};

        leftImgBit = leftImgDrawable.getBitmap();
        float w = leftImgBit.getWidth();
        float h = leftImgBit.getWidth();
        Matrix matrix = new Matrix();

        //图片大小
        imgSize = 16;
        float scale = Math.min(PixelUtil.getPx(imgSize)/w,PixelUtil.getPx(imgSize)/h);
        matrix.setScale(scale,scale);
        leftImgBit = Bitmap.createBitmap(leftImgBit,0,0,(int)w,(int)h,matrix,false);

        rightImgBit = rightImgDrawable.getBitmap();
        w = rightImgBit.getWidth();
        h = rightImgBit.getWidth();
        matrix = new Matrix();
        scale = Math.min(PixelUtil.getPx(imgSize)/w,PixelUtil.getPx(imgSize)/h);
        matrix.setScale(scale,scale);
        rightImgBit = Bitmap.createBitmap(rightImgBit,0,0,(int)w,(int)h,matrix,false);

        paint = new Paint();
        paint.reset();
        paint.setTextSize(TextSize.TITLE.getTextSize());
        paint.setTypeface(Typeface.DEFAULT);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        if(widthMode == MeasureSpec.EXACTLY){
            width = widthSize;
            item = width/6;
            Paint.FontMetrics fontMetrics=paint.getFontMetrics();
            float distance=(fontMetrics.bottom - fontMetrics.top)/2 - fontMetrics.bottom;
            baseline=height/2+distance;
        }
        if(heightMode == MeasureSpec.EXACTLY){
            height = heightSize;
        }

        setMeasuredDimension(width,height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        this.paint.setColor(DFColor.DF_BLUE.getColor());

        if(rectF == null) {
            rectF =  new RectF(0,0,width,height);
        }

        //绘制背景
        canvas.drawRect(rectF,paint);

        this.paint.setColor(DFColor.WHITE.getColor());
        int end = titles.length;
        for(int i = 0;i < end;i++) {
            float startShow = item * (i+1);
            float len = this.paint.measureText(titles[i]);
            startShow = startShow + (item/2-len/2);
            canvas.drawText(titles[i],startShow,baseline,paint);
        }

        canvas.drawBitmap(leftImgBit,(item-PixelUtil.getPx(imgSize))/2,(height-PixelUtil.getPx(imgSize))/2,paint);
        canvas.drawBitmap(rightImgBit,item*5 + (item-PixelUtil.getPx(imgSize))/2,(height-PixelUtil.getPx(imgSize))/2,paint);

        super.onDraw(canvas);
    }
}
