package com.gxuwz.df.ui.activity;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.gxuwz.df.ui.fragment.BaseFragment;

import java.util.List;

public class FragmentAdapter extends FragmentPagerAdapter {

    List<BaseFragment> fragmentList;

    public FragmentAdapter(@NonNull FragmentManager fm, List<BaseFragment> fragmentList) {
        super(fm);
        this.fragmentList = fragmentList;
    }

    public FragmentAdapter(@NonNull FragmentManager fm, int behavior, List<BaseFragment> fragmentList) {
        super(fm, behavior);
        this.fragmentList = fragmentList;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }
}
