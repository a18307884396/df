package com.gxuwz.df.ui.activity;

import com.gxuwz.df.ui.widget.NavIndexTop;

public abstract class NavBaseActivity extends BaseActivity{
    protected NavIndexTop navIndexTop;
}

/**
 * <com.gxuwz.df.ui.widget.NavIndexTop
 *         android:id="@+id/nav_top"
 *         android:layout_width="match_parent"
 *         android:layout_height="50dp"
 *         app:leftImg="@drawable/icon_more_mw"
 *         app:rightImg="@drawable/icon_more_mw"
 *         app:layout_constraintTop_toTopOf="parent"
 *         app:title1="我的"
 *         app:title2="发现"
 *         app:title3="云村"
 *         app:title4="视频" />
 */
