package com.gxuwz.df.ui.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    private final static String simpleFormat = "yyyy-MM-dd HH:mm:ss";

    public static String format(String dateFormat){
        SimpleDateFormat df = new SimpleDateFormat(dateFormat);
        return df.format(new Date());
    }

    public static String formatSimple(){
        SimpleDateFormat df = new SimpleDateFormat(simpleFormat);
        return df.format(new Date());
    }

}
