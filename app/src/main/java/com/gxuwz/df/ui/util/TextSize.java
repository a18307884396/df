package com.gxuwz.df.ui.util;

import com.gxuwz.df.ui.util.PixelUtil;

public enum TextSize {

    TITLE(PixelUtil.getPx(16),"标题"),
    THEME_TEXT(PixelUtil.getPx(14),"主题字体大小"),
    TEXT_SIZE_2(PixelUtil.getPx(12),"二级主题字体大小");

    private int textSize;
    private String remark;

    TextSize(int textSize,String remark){
        this.textSize = textSize;
        this.remark = remark;
    }

    public int getTextSize(){
        return this.textSize;
    }
}
