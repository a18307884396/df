package com.gxuwz.df.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.gxuwz.df.R;
import com.gxuwz.df.business.model.entity.Songlist;
import com.gxuwz.df.ui.adapter.SonglistAdapter;

import java.util.List;

public class TitleListView extends ConstraintLayout {

    private String title;
    private List<Songlist> songlists;

    Context context;
    TextView titleTv;
    RecyclerView songlistRv;
    SonglistAdapter songlistAdapter;

    public TitleListView(@NonNull Context context) {
        super(context);
    }

    public TitleListView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs);
    }

    public TitleListView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context,attrs);
    }

    public TitleListView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context,attrs);
    }

    private void init(Context context,AttributeSet attrs){
        this.context = context;
        LayoutInflater.from(context).inflate(R.layout.user_sl_view,this,true);
        titleTv = findViewById(R.id.title_tv);
        songlistRv = findViewById(R.id.song_list_rv);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TitleListView);
        title = typedArray.getString(R.styleable.TitleListView_slTitle);
        titleTv.setText(title);
    }

    public void setSonglists(List<Songlist> songlists) {
        this.songlists = songlists;
        if(songlistAdapter == null){
            songlistRv.setLayoutManager(new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL));
            songlistAdapter = new SonglistAdapter(context,songlists);
            songlistRv.setAdapter(songlistAdapter);
        } else {
            songlistAdapter.updateList(this.songlists);
        }
    }

    @Override
    public void destroyDrawingCache() {
        this.context = null;
        songlistAdapter.destory();
        super.destroyDrawingCache();
    }
}
