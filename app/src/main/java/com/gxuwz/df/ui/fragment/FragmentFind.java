package com.gxuwz.df.ui.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.gxuwz.df.R;
import com.gxuwz.df.business.utils.comm.PlayCommand;
import com.gxuwz.df.ui.fragment.viewconponent.FindViewConponent;

public class FragmentFind extends BaseFragment {

    FindViewConponent views;

    public FragmentFind() {
        super(new PlayCommand());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_find, container, false);
    }

    @Override
    public void initView() {
        views = new FindViewConponent(getActivity());
    }

    @Override
    public void service() {

    }

    @Override
    public void cleartData() {

    }
}
