package com.gxuwz.df.ui.fragment.viewconponent;

import android.app.Activity;
import android.widget.Spinner;

import com.gxuwz.df.R;
import com.gxuwz.df.ioc.DfResource;
import com.gxuwz.df.ioc.inject.ClassViewInject;
import com.gxuwz.df.ui.adapter.StuSpinnerAdapter;

import java.util.ArrayList;
import java.util.List;

public class VideoViewConponent {
    @DfResource(R.id.stu_spinner)
    public Spinner stuSpinner;

    public VideoViewConponent(Activity activity) {
        ClassViewInject.inject(activity,this);
        initStuSpinner(activity);
    }

    public void initStuSpinner(Activity activity){
        List<String> stuList = new ArrayList<>();
        stuList.add("zhouriyue");
        stuList.add("liming");
        stuList.add("wangming");
        StuSpinnerAdapter stuSpinnerAdapter = new StuSpinnerAdapter(activity,stuList);
        stuSpinner.setAdapter(stuSpinnerAdapter);
    }
}
