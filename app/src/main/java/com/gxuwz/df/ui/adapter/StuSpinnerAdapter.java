package com.gxuwz.df.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gxuwz.df.R;

import java.util.List;

public class StuSpinnerAdapter extends BaseAdapter {

    LayoutInflater inflater;
    List<String> stuList;

    public StuSpinnerAdapter(Context context, List<String> stuList) {
        this.inflater = LayoutInflater.from(context);
        this.stuList = stuList;
    }

    @Override
    public int getCount() {
        return stuList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = inflater.inflate(R.layout.item_spinner_stu,null);

        TextView titleTv = view.findViewById(R.id.title_tv);
        titleTv.setText(stuList.get(position));

        return view;
    }
}
