package com.gxuwz.df.ui.activity;

import android.graphics.drawable.BitmapDrawable;

import com.gxuwz.df.R;
import com.gxuwz.df.ui.widget.BottomPlayView;
import com.gxuwz.df.ui.widget.NavIndexTop;

/**
 * 包含头部导航和底部播放器的Activity
 */
public abstract class NavBtmPlayActivity extends BaseActivity{

    protected NavIndexTop navIndexTop;
    protected BottomPlayView bottomPlayView;

    protected void init(){
        findViews();
        initNav();
        initBtmPv();
    }

    protected void initNav(){

    }

    protected void findViews(){
        navIndexTop = findViewById(R.id.nav_top);
        bottomPlayView = findViewById(R.id.bottom_play_view);
    }

    /** 初始化 **/
    protected void initBtmPv(){
        BitmapDrawable bitmapDrawable = (BitmapDrawable)getResources().getDrawable(R.drawable.img_my);
        bottomPlayView.setSongImg(bitmapDrawable.getBitmap());
        bottomPlayView.setSongName("浪漫手机");
        bottomPlayView.setSingerName("周杰伦");
    }
}
