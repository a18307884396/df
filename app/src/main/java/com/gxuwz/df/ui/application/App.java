package com.gxuwz.df.ui.application;

import android.app.Application;
import android.content.pm.PackageManager;
import android.os.Environment;

import com.alipay.euler.andfix.patch.PatchManager;
import com.gxuwz.df.ioc.IOC;
import com.gxuwz.df.ui.util.LogUtil;
import com.gxuwz.df.ui.util.PixelUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.SignatureException;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        PixelUtil.init(this);
        IOC.initIOC(this);

        PatchManager patchManager = new PatchManager(getApplicationContext());
        try {
            patchManager.init(getPackageManager().getPackageInfo(this.getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        try{
            patchManager.loadPatch();
        }catch (Exception e){
            LogUtil.log("loadPatch()","load error!");
        }


        String patchPath = Environment.getExternalStorageDirectory()
                .getAbsolutePath() + "/patch.apatch";
        try {
            patchManager.addPatch(patchPath);
        } catch (IOException e) {
            LogUtil.log("app",e.getMessage());
        }
    }
}
