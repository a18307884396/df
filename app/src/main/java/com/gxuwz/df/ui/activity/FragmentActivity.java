package com.gxuwz.df.ui.activity;

import android.view.View;

import androidx.viewpager.widget.ViewPager;

import com.gxuwz.df.R;
import com.gxuwz.df.ui.fragment.BaseFragment;
import com.gxuwz.df.ui.listener.PagerListener;

import java.util.ArrayList;
import java.util.List;

public abstract class FragmentActivity extends NavBtmPlayActivity{

    protected ViewPager viewPager;
    protected List<BaseFragment> fragmentList;
    protected FragmentAdapter fragmentAdapter;
    protected View translateView;

    @Override
    protected void init() {
        super.init();
        initViewPager();
    }

    private void initViewPager(){
        viewPager = findViewById(R.id.view_pager);
        fragmentList = new ArrayList<>();
        fragmentAdapter = new FragmentAdapter(getSupportFragmentManager(),fragmentList);

        translateView = findViewById(R.id.translate_view);
        viewPager.setOnPageChangeListener(new PagerListener(translateView,6));
    }
    
    public void addFragment(BaseFragment fragment){
        fragmentList.add(fragment);
        viewPager.setOffscreenPageLimit(3);
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewPager.setAdapter(fragmentAdapter);
        viewPager.setCurrentItem(0);
    }
}
