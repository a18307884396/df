package com.gxuwz.df.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

public abstract class BaseAdapter<T,F extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<F> {

    Context context;
    List<T> list;

    public BaseAdapter(Context context,List<T> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void updateList(List<T> list){
        this.list = list;
        notifyDataSetChanged();
    }

    public void destory(){
        this.context = null;
        this.list = null;
    }
}
