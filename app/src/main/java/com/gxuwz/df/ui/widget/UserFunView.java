package com.gxuwz.df.ui.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import com.gxuwz.df.business.model.vo.UserFunVo;
import com.gxuwz.df.ui.util.BitmapUtil;
import com.gxuwz.df.ui.util.DFColor;
import com.gxuwz.df.ui.util.DFPaint;
import com.gxuwz.df.ui.util.PixelUtil;
import com.gxuwz.df.ui.util.TextSize;

import java.util.List;

/**
 * 用户功能View
 */
public class UserFunView extends View {

    int width;
    int height;
    private List<UserFunVo> userFunVos;
    private Paint paint = new Paint();

    private float itemWidth;
    private float imgLeftPadding;
    private int imgTopPadding;
    private int textPadding;
    private int imgSize = 30;
    private int textSize = TextSize.THEME_TEXT.getTextSize();

    public UserFunView(Context context) {
        super(context);
        init();
    }

    public UserFunView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public UserFunView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public UserFunView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private void init(){
        paint.setTextSize(textSize);
        paint.setColor(DFColor.DF_2C.getColor());
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        if(widthMode == MeasureSpec.EXACTLY){
            this.width = widthSize;
            this.itemWidth = width/userFunVos.size();
            this.imgLeftPadding = this.itemWidth/2 - PixelUtil.getPx(imgSize)/2;
            this.imgTopPadding = (height - PixelUtil.getPx(imgSize+textSize))/2;
            this.textPadding = (int) (PixelUtil.getPx(imgTopPadding+imgSize) + DFPaint.measureBase(paint));
        }
        if(heightMode == MeasureSpec.EXACTLY){
            this.height = heightSize;
        }
        setMeasuredDimension(this.width,this.height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if(userFunVos != null){
            int len = userFunVos.size();
            for(int i = 0;i < len;i++){
                UserFunVo userFunVo = userFunVos.get(i);
                float layoutW = itemWidth * i + imgLeftPadding;
                Bitmap bitmap = BitmapUtil.getBitmap(userFunVo.getFunIcon(),imgSize,imgSize);
                canvas.drawBitmap(bitmap,layoutW, imgTopPadding,paint);
                float textLen = paint.measureText(userFunVo.getFunName());
                canvas.drawText(userFunVo.getFunName(),itemWidth * i +
                        itemWidth/2 - textLen/2,textPadding,paint);
            }
        }
        super.onDraw(canvas);
    }

    public void setUserFunVos(List<UserFunVo> userFunVos) {
        this.userFunVos = userFunVos;
    }
}
