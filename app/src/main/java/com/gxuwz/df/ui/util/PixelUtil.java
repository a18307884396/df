package com.gxuwz.df.ui.util;

import android.content.Context;

public class PixelUtil {

    public static int width_dp;
    public static int height_dp;

    public static int width_px;
    public static int height_px;

    public static float scale;

    public static void init(Context context){
        scale = context.getResources().getDisplayMetrics().density;
        width_px = context.getResources().getDisplayMetrics().widthPixels;
        height_px = context.getResources().getDisplayMetrics().heightPixels;
    }

    public static int getPx(int db){
        return (int) (db*scale+0.5f);
    }

}
