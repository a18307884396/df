package com.gxuwz.df.ui.fragment.viewconponent;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.gxuwz.df.R;
import com.gxuwz.df.ioc.DfResource;
import com.gxuwz.df.ioc.inject.ClassViewInject;
import com.gxuwz.df.ui.util.ChatUtil;

public class FindViewConponent {

    @DfResource(R.id.message_et)
    public EditText messageEt;
    @DfResource(R.id.sub_btn)
    public Button subBtn;
    @DfResource(R.id.mess_tv)
    public TextView messTv;

    ChatUtil chatUtil;

    Handler handler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if(msg.what == 1){
                messTv.append(msg.obj.toString()+"\n");
                messageEt.setText("");
            }
        }
    };

    public FindViewConponent(Activity activity) {
        ClassViewInject.inject(activity,this);
        /*chatUtil = ChatUtil.getInstance("192.168.137.1", 30003,handler);*/
    }

    public void setSendListenr(){
        subBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chatUtil.sendMess(messageEt.getText().toString());
            }
        });
    }
}
