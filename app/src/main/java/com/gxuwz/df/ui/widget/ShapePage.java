package com.gxuwz.df.ui.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import com.gxuwz.df.ui.util.DFColor;
import com.gxuwz.df.ui.util.PixelUtil;

public class ShapePage extends View {

    int width;
    int height;
    RectF rectF;
    Paint paint = new Paint();

    public ShapePage(Context context) {
        super(context);
        init();
    }

    public ShapePage(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ShapePage(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public ShapePage(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init(){
        paint.setStrokeWidth(PixelUtil.getPx(1));
        paint.setColor(DFColor.DF_70.getColor());
        paint.setStyle(Paint.Style.STROKE);
        paint.setAntiAlias(true);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        if(widthMode == MeasureSpec.EXACTLY){
            width = widthSize;
        }
        if(heightMode == MeasureSpec.EXACTLY){
            height = heightSize;
        }
        setMeasuredDimension(width,height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if(rectF == null){
            rectF = new RectF(PixelUtil.getPx(1),PixelUtil.getPx(1), height,height);
        }
        canvas.drawArc(rectF,180,90,false,paint);
        rectF = new RectF(width-height,PixelUtil.getPx(1),width,height);
        canvas.drawLine(height/2,PixelUtil.getPx(1),width-height/2,PixelUtil.getPx(1),paint);
        canvas.drawArc(rectF,270,90,false,paint);
        super.onDraw(canvas);
    }
}
