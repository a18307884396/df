package com.gxuwz.df.ui.listener;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;

import androidx.viewpager.widget.ViewPager;

import com.gxuwz.df.business.model.entity.User;
import com.gxuwz.df.ioc.IOC;
import com.gxuwz.df.ui.util.PixelUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

public class PagerListener implements ViewPager.OnPageChangeListener {

    int item;
    int last = 0;
    View view;

    HashMap<String,Object> animationHash = new HashMap<>();
    AnimationSet set = new AnimationSet(true);


    public PagerListener(View view,int size) {
        this.view = view;
        item = PixelUtil.width_px/size;
        set.setFillAfter(true);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        set.getAnimations().clear();

        //平移
        String translateName = "translate"+item*last+"To"+item*position;
        TranslateAnimation translate = (TranslateAnimation) animationHash.get(translateName);
        if(translate == null){

            translate = new TranslateAnimation(item*last,item*position,0,0);
            translate.setDuration(700);
            animationHash.put(translateName,translate);
        }
        set.addAnimation(translate);


        //缩放
        String scaleName = "scale"+1+","+0.5f+"To"+1+","+0.5f;
        ScaleAnimation scale = (ScaleAnimation) animationHash.get(scaleName);
        if(scale == null){
            scale = new ScaleAnimation(1,0.5f,1,0.5f,Animation.RELATIVE_TO_SELF,0.5f,Animation.RELATIVE_TO_SELF,0.5f);
            scale.setDuration(700);
        }
        set.addAnimation(scale);

        //缩放
        scaleName = "scale"+1+","+2f+"To"+1+","+2f;
        scale = (ScaleAnimation) animationHash.get(scaleName);
        if(scale == null){
            scale = new ScaleAnimation(1,2f,1,2f,Animation.RELATIVE_TO_SELF,0.5f,Animation.RELATIVE_TO_SELF,0.5f);
            scale.setDuration(700);
        }
        set.addAnimation(scale);

        last = position;
        view.startAnimation(set);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
