package com.gxuwz.df.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.gxuwz.df.R;
import com.gxuwz.df.business.utils.comm.PlayCommand;
import com.gxuwz.df.ui.fragment.viewconponent.VideoViewConponent;

public class FragmentVideo extends BaseFragment {

    VideoViewConponent views;

    public FragmentVideo() {
        super(null);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_video,container,false);
    }

    @Override
    public void initView() {
        views = new VideoViewConponent(getActivity());
    }

    @Override
    public void service() {

    }

    @Override
    public void cleartData() {

    }
}
