package com.gxuwz.df.ui.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

public class BitmapUtil {

    public static Bitmap getBitmap(Context context,int layoutId){
        Drawable drawable = context.getDrawable(layoutId);
        BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
        return bitmapDrawable.getBitmap();
    }

    public static Bitmap getBitmap(Bitmap bitmap,int wdp,int hdp){
        Matrix matrix = new Matrix();
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();
        float midW = PixelUtil.getPx(wdp);
        float midH = PixelUtil.getPx(hdp);
        float scale = Math.min(midW/w,midH/h);
        matrix.setScale(scale,scale);
        bitmap = Bitmap.createBitmap(bitmap,0,0,w,h,matrix,false);
        return bitmap;
    }

    public static Bitmap getBitmap(Context context,int layoutId,int wdp,int hdp){
        Bitmap bitmap = getBitmap(context,layoutId);

        Matrix matrix = new Matrix();
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();
        float midW = PixelUtil.getPx(wdp);
        float midH = PixelUtil.getPx(hdp);
        float scale = Math.min(midW/w,midH/h);
        matrix.setScale(scale,scale);
        bitmap = Bitmap.createBitmap(bitmap,0,0,w,h,matrix,false);

        return bitmap;
    }

}
