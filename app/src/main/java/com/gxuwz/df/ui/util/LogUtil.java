package com.gxuwz.df.ui.util;

import android.os.Environment;
import android.text.format.DateUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.nio.charset.StandardCharsets;

public class LogUtil {

    public static String basePath = Environment.getExternalStorageDirectory()
            .getAbsolutePath() + "/log/df/";

    public static LogUtil instance = null;

    static {
        instance = new LogUtil();
    }

    public static LogUtil getInstance() {
        return instance;
    }

    public static void log(String topic,String message){
        String fileName = basePath+
                DateUtil.format("yyyy-MM-dd")+".txt";
        File logFile = new File(fileName);
        if(!logFile.exists()){
            logFile.getParentFile().mkdir();
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            OutputStream outputStream = new FileOutputStream(logFile,false);
            String content = DateUtil.formatSimple() + "//" +topic+ ":" +message+"\n";
            outputStream.write(content.getBytes(StandardCharsets.UTF_8));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
