package com.gxuwz.df.ui.util;

import android.graphics.Paint;

import androidx.annotation.NonNull;

/**
 * 多芬画笔
 */
public class DFPaint extends Paint implements Cloneable {

    public static float measureBase(Paint paint){
        FontMetrics fontMetrics = paint.getFontMetrics();
        float base = (fontMetrics.bottom - fontMetrics.top)/2 - fontMetrics.bottom;
        return base;
    }
}
