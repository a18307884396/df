package com.gxuwz.df.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gxuwz.df.R;
import com.gxuwz.df.business.model.entity.Songlist;

import java.util.List;

public class SonglistAdapter extends BaseAdapter<Songlist,SonglistAdapter.SonglistViewHolder> {

    public SonglistAdapter(Context context,List<Songlist> list) {
        super(context,list);
    }

    @NonNull
    @Override
    public SonglistViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SonglistViewHolder(LayoutInflater.from(context).inflate(R.layout.item_songlist,null));
    }

    @Override
    public void onBindViewHolder(@NonNull SonglistViewHolder holder, int position) {
        Songlist songlist = list.get(position);
        Glide.with(context)
                .load(Integer.valueOf(songlist.getSonglistUrl()))
                .into(holder.songlistIv);
        holder.titleTv.setText(songlist.getSonglistName());
        holder.songNumberTv.setText(songlist.getSongNumber()+"首");
    }

    protected static class SonglistViewHolder extends RecyclerView.ViewHolder{

        ImageView songlistIv;
        TextView titleTv;
        TextView songNumberTv;

        public SonglistViewHolder(@NonNull View itemView) {
            super(itemView);
            songlistIv = itemView.findViewById(R.id.songlist_iv);
            titleTv = itemView.findViewById(R.id.sl_title_tv);
            songNumberTv = itemView.findViewById(R.id.song_num_tv);
        }
    }
}
