package com.gxuwz.df.ui.fragment.viewconponent;

import android.app.Activity;
import android.widget.Button;
import android.widget.EditText;

import com.gxuwz.df.R;
import com.gxuwz.df.ioc.DfResource;
import com.gxuwz.df.ioc.inject.ClassViewInject;
import com.gxuwz.df.ioc.inject.FragmentFieldInject;

public class YunViewConponent {
    @DfResource(R.id.mess_et)
    public EditText messEt;
    @DfResource(R.id.send_btn)
    public Button sendBtn;

    public YunViewConponent(Activity activity) {
        ClassViewInject.inject(activity,this);
    }
}
