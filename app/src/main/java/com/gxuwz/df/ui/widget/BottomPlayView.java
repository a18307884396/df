package com.gxuwz.df.ui.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import com.gxuwz.df.R;
import com.gxuwz.df.ioc.IOC;
import com.gxuwz.df.ui.util.DFColor;
import com.gxuwz.df.ui.util.TextSize;
import com.gxuwz.df.ui.util.BitmapUtil;
import com.gxuwz.df.ui.util.DFPaint;
import com.gxuwz.df.ui.util.PixelUtil;

public class BottomPlayView extends View {

    private int width;
    private int height;

    Paint paint;

    private Bitmap songImg;//歌曲图片
    private String songName;//歌曲名
    private String singerName;//歌手名

    private boolean playState;//播放状态

    private Bitmap playImg;//播放图标
    private Bitmap stopImg;//暂停图标
    private Bitmap playListImg;//播放列表图标

    public BottomPlayView(Context context) {
        super(context);
    }

    public BottomPlayView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public BottomPlayView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public BottomPlayView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context){
        paint = new Paint();
        playImg = BitmapUtil.getBitmap(context,R.drawable.icon_play_circle,24,24);
        stopImg = BitmapUtil.getBitmap(context,R.drawable.icon_stop_circle,24,24);
        playListImg = BitmapUtil.getBitmap(context,R.drawable.icon_play_list,24,24);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        if(widthMode == MeasureSpec.EXACTLY){
            width = widthSize;
        }
        if(heightMode == MeasureSpec.EXACTLY){
            height = heightSize;
        }
        setMeasuredDimension(width,height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        //绘制上画线
        paint.setColor(DFColor.GRAY_FRA.getColor());
        canvas.drawLine(0,0,width,PixelUtil.getPx(1),paint);

        //绘制歌曲图片
        if(songImg != null) {
            canvas.drawBitmap(songImg,PixelUtil.getPx(2),PixelUtil.getPx(2),paint);
        }

        //绘制歌曲名
        if(songName != null) {
            paint.setColor(DFColor.DF_2C.getColor());
            float midW = PixelUtil.getPx(48) + 5;
            paint.setTextSize(TextSize.THEME_TEXT.getTextSize());
            Paint.FontMetrics fontMetrics = paint.getFontMetrics();
            float midY = -fontMetrics.top + 5;
            canvas.drawText(songName,midW,1+midY,paint);
        }

        //绘制歌手名
        if(singerName != null) {
            paint.setColor(DFColor.DF_70.getColor());
            float midW = PixelUtil.getPx(48) + 5;
            paint.setTextSize(TextSize.TEXT_SIZE_2.getTextSize());
            Paint.FontMetrics fontMetrics = paint.getFontMetrics();
            float midY = height - fontMetrics.bottom - 5;
            canvas.drawText(songName,midW,midY,paint);
        }

        //绘制播放图标
        int midX = width - PixelUtil.getPx(84);
        int midY = PixelUtil.getPx(13);
        if(playState){
            canvas.drawBitmap(stopImg,midX,midY,paint);
        } else {
            canvas.drawBitmap(playImg,midX,midY,paint);
        }

        midX = width - PixelUtil.getPx(36);
        midY = PixelUtil.getPx(13);
        canvas.drawBitmap(playListImg,midX,midY,paint);
        super.onDraw(canvas);
    }

    public void setSongImg(Bitmap songImg) {
        Matrix matrix = new Matrix();
        int w = songImg.getWidth();
        int h = songImg.getHeight();
        float midW = PixelUtil.getPx(44);
        float midH = PixelUtil.getPx(44);
        float scale = Math.min(midW/w,midH/h);
        matrix.setScale(scale,scale);
        this.songImg = Bitmap.createBitmap(songImg,0,0,w,h,matrix,false);
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public void setSingerName(String singerName) {
        this.singerName = singerName;
    }

    public boolean isPlayState() {
        return playState;
    }

    public void setPlayState(boolean playState) {
        this.playState = playState;
    }
}
