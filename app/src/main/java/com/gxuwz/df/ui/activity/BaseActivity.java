package com.gxuwz.df.ui.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

/**
 * Activity抽象类
 */
public abstract class BaseActivity extends AppCompatActivity{

    public Intent intent = new Intent();

    protected abstract void init();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * 跳转到新的页面
     * @param clazz
     */
    public void startNewActivity(Class clazz){
        intent.setClass(this,clazz);
        this.startActivity(intent);
    }

    //销毁数据
    public abstract void destroyData();

    @Override
    protected void onDestroy() {
        intent = null;
        destroyData();//销毁数据
        super.onDestroy();
    }
}
