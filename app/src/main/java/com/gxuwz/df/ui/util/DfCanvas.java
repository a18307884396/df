package com.gxuwz.df.ui.util;

import android.graphics.Canvas;

public class DfCanvas extends Canvas implements Cloneable{

    public static DfCanvas instance = new DfCanvas();

    public static DfCanvas getInstance() {
        return instance;
    }

    public static DfCanvas cloneDf(){
        try {
            return (DfCanvas) instance.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }
}
