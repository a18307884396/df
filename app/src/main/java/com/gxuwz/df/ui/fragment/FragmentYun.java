package com.gxuwz.df.ui.fragment;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.gxuwz.df.R;
import com.gxuwz.df.business.utils.comm.PlayCommand;
import com.gxuwz.df.ui.fragment.viewconponent.YunViewConponent;
import com.gxuwz.df.ui.service.MQTTService;

public class FragmentYun extends BaseFragment {

    YunViewConponent yunViewConponent;

    public FragmentYun() {
        super(new PlayCommand());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_yun,container,false);
    }

    @Override
    public void initView() {
        yunViewConponent = new YunViewConponent(getActivity());
        yunViewConponent.sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MQTTService.publish("zhouriyue");
            }
        });
    }

    @Override
    public void service() {
        //MQTTService.startService(getContext()); //开启服务
    }


    @Override
    public void cleartData() {
    }
}
