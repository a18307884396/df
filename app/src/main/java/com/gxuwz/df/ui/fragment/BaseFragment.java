package com.gxuwz.df.ui.fragment;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.gxuwz.df.business.utils.comm.BaseCommand;
import com.gxuwz.df.business.model.entity.Song;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

public abstract class BaseFragment extends Fragment {

    BaseCommand baseCommand;

    public BaseFragment(BaseCommand baseCommand){
        this.baseCommand = baseCommand;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        service();
    }

    public abstract void initView();//初始化页面

    public abstract void service();//业务

    public void play(Song song){
        getContext().sendBroadcast(baseCommand.getPlayCommand(song));
    }

    public void stop(Song song){
        getContext().sendBroadcast(baseCommand.getStopCommand(song));
    }

    public void fieldInvoke(String fieldName,String viewName,String methodName,List list){
        try {
            Field field = this.getClass().getDeclaredField(fieldName);
            Class fieldClass = field.getType();
            Field view = fieldClass.getDeclaredField(viewName);
            Class viewClass = view.getType();
            Method method = viewClass.getDeclaredMethod(methodName,List.class);

            Object fieldObj = field.get(this);
            method.invoke(view.get(fieldObj),list);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public void fieldInvoke(String fieldName,String viewName,String methodName,Object obj){
        try {
            Field field = this.getClass().getDeclaredField(fieldName);
            Class fieldClass = field.getType();
            Field view = fieldClass.getDeclaredField(viewName);
            Class viewClass = view.getType();
            Method method = viewClass.getDeclaredMethod(methodName,obj.getClass());

            Object fieldObj = field.get(this);
            method.invoke(view.get(fieldObj),obj);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public void fieldInvoke(String filedName, String methodName, List list){
        try {
            Field field = this.getClass().getDeclaredField(filedName);
            Class fieldClass = field.getType();
            Method method = fieldClass.getMethod(methodName,list.getClass());
            method.invoke(field.get(this),list);
        } catch (NoSuchFieldException | NoSuchMethodException
                | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public void fieldInvoke(String filedName, String methodName, Object object){
        try {
            Field field = this.getClass().getDeclaredField(filedName);
            Class fieldClass = field.getType();
            Method method = fieldClass.getMethod(methodName,object.getClass());
            method.invoke(field.get(this),object);
        } catch (NoSuchFieldException | NoSuchMethodException
                | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /** 代理模式 **/
    public void methodInvoke(String methodName, Object object){
        try {
            Method method = this.getClass().getMethod(methodName,object.getClass());
            method.invoke(this,object);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cleartData();
    }

    public abstract void cleartData();
}
