package com.gxuwz.df.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.gxuwz.df.R;
import com.gxuwz.df.business.service.PersonService;
import com.gxuwz.df.business.service.SonglistService;
import com.gxuwz.df.business.service.UserService;
import com.gxuwz.df.ioc.Auto;
import com.gxuwz.df.ioc.IOC;
import com.gxuwz.df.ioc.inject.FragmentFieldInject;
import com.gxuwz.df.ui.fragment.viewconponent.MyViewConponent;

public class FragmentMy extends BaseFragment {

    MyViewConponent views;

    @Auto
    UserService userService;

    @Auto
    SonglistService songlistService;

    @Auto
    PersonService personService;

    public FragmentMy() {
        super(null);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my,container,false);
    }


    @Override
    public void initView() {
        FragmentFieldInject fragmentFieldInject = (FragmentFieldInject) IOC.get("FragmentFieldInject");
        fragmentFieldInject.inject(this);

        views = new MyViewConponent(getActivity());
    }

    @Override
    public void service() {
        userService.findById(this);
        userService.findFun(this);

        songlistService.findUserSl(this);

        personService.findAll();
    }

    @Override
    public void cleartData() {
    }


}
