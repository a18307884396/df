package com.gxuwz.df.ioc.inject;

public abstract class AbstractInject<T> {
    public abstract void inject(T t);
    public T chechNull(T t, String message){
        if(t == null){
            throw new NullPointerException(message);
        }
        return t;
    }
}
