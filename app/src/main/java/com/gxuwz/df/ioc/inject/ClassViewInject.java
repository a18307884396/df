package com.gxuwz.df.ioc.inject;

import android.app.Activity;
import android.view.View;

import com.gxuwz.df.ioc.DfResource;

import java.lang.reflect.Field;

public class ClassViewInject {

    public static void inject(Activity activity, Object object) {
        try {
            if (activity != null && object != null) {
                Class clazz = object.getClass();
                Field[] fields = clazz.getDeclaredFields();
                for (Field field : fields) {
                    DfResource dfResource = field.getAnnotation(DfResource.class);
                    if (dfResource != null) {
                        int layoutId = dfResource.value();
                        View view = activity.findViewById(layoutId);
                        field.setAccessible(true);
                        field.set(object, view);
                    }
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

}
