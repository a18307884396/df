package com.gxuwz.df.ioc.inject;

import android.view.View;

import androidx.fragment.app.Fragment;

import com.gxuwz.df.ioc.Auto;
import com.gxuwz.df.ioc.DfBean;
import com.gxuwz.df.ioc.DfResource;
import com.gxuwz.df.ioc.IOC;

import java.lang.reflect.Field;

@DfBean
public class FragmentFieldInject extends AbstractInject<Fragment> {

    @Override
    public void inject(Fragment fragment) {
        try {
            if (fragment instanceof Fragment) {
                Class clazz = fragment.getClass();
                Field[] fields = clazz.getDeclaredFields();
                for (Field field:fields) {
                    DfResource dfResource = field.getAnnotation(DfResource.class);
                    Auto auto = field.getAnnotation(Auto.class);
                    if (dfResource != null) {
                        int layoutId = dfResource.value();
                        View view = ((Fragment)fragment).getView().findViewById(layoutId);
                        field.setAccessible(true);
                        field.set(fragment,view);
                    } else if(auto != null){
                        String simpleName = field.getType().getSimpleName();
                        Object value = IOC.get(simpleName);
                        if(value == null){
                            throw new NullPointerException(simpleName+" not within IOC.");
                        }
                        field.setAccessible(true);
                        field.set(fragment,value);
                    }
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
