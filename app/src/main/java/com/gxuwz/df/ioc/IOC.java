package com.gxuwz.df.ioc;

import android.content.Context;

import com.gxuwz.df.business.greendao.DaoMaster;
import com.gxuwz.df.business.greendao.DaoSession;
import com.gxuwz.df.business.utils.comm.AbstractCloneable;

import org.greenrobot.greendao.AbstractDao;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;

import dalvik.system.DexFile;

/**
 * IOC容器
 */
public class IOC {

    public static final String fileName = "bean.properties";
    private static final String scan = "ScanPackage";

    private static boolean isInit = false;

    private static HashMap<String, Object> ioc = new HashMap<String, Object>((int) (100/0.75f+1));

    static class GreenDaoBase{
        DaoMaster.DevOpenHelper helper;
        DaoMaster master;
        DaoSession session;

        public void clear(){
            this.helper = null;
            this.master = master;
            this.session = session;
        }
    }

    public final static void initIOC(Context context) {
        if (!isInit) {
            try {
                GreenDaoBase greenDaoBase = new GreenDaoBase();
                greenDaoBase.helper = new DaoMaster.DevOpenHelper(context,"df.db",null);
                greenDaoBase.master = new DaoMaster(greenDaoBase.helper.getWritableDatabase());
                greenDaoBase.session = greenDaoBase.master.newSession();

                Properties properties = new Properties();
                properties.load(context.getAssets().open(fileName));
                String scanPackages = properties.getProperty(scan);

                DexFile dexFile = new DexFile(context.getPackageResourcePath());
                Enumeration classEn = dexFile.entries();

                if (!scanPackages.contains(",")) {
                    while (classEn.hasMoreElements()) {
                        String className = (String) classEn.nextElement();
                        if (className.contains(scanPackages)) {
                            saveInstance(className,greenDaoBase);
                        }
                    }
                } else {
                    String[] packages = scanPackages.split(",");
                    while (classEn.hasMoreElements()) {
                        String className = (String) classEn.nextElement();
                        for (String pg : packages) {
                            if (className.contains(pg)) {
                                saveInstance(className,greenDaoBase);
                                break;
                            }
                        }
                    }
                }

                greenDaoBase.clear();
                greenDaoBase = null;

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                isInit = true;
            }
        }
    }

    private static void saveInstance(String className,GreenDaoBase greenDaoBase) {
        try {
            Class clazz = null;
            clazz = Class.forName(className);
            if (clazz != null) {
                Annotation bean = clazz.getAnnotation(DfBean.class);
                if (bean != null) {
                    Object object = IOC.get(clazz.getSimpleName());
                    if(object == null){
                        if(clazz.getSuperclass() == AbstractDao.class){
                            String simpleName = clazz.getSimpleName();
                            Object daoObj = IOC.get(simpleName);
                            if(daoObj == null){
                                Class sessionClass = greenDaoBase.session.getClass();
                                Method getDaoMethod = sessionClass.getMethod("get"+clazz.getSimpleName());
                                object = getDaoMethod.invoke(greenDaoBase.session);
                                IOC.put(simpleName,object);
                            }
                        } else {
                            object = clazz.newInstance();
                            put(clazz.getSimpleName(),object);
                        }
                    }
                    saveFieldInstance(object,clazz,greenDaoBase);
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private static void saveFieldInstance(Object obj, Class clazz,GreenDaoBase greenDaoBase) {
        try {
            Field fields[] = clazz.getDeclaredFields();
            for (Field field : fields) {
                Auto auto = field.getAnnotation(Auto.class);
                if (auto != null) {
                    Class fieldClass = field.getType();
                    Object fieldObj = IOC.get(fieldClass.getSimpleName());
                    if(fieldObj == null){
                        if(fieldClass.getSuperclass() == AbstractDao.class){
                            String simpleName = fieldClass.getSimpleName();
                            Object daoObj = IOC.get(simpleName);
                            if(daoObj == null){
                                Class sessionClass = greenDaoBase.session.getClass();
                                Method getDaoMethod = sessionClass.getMethod("get"+fieldClass.getSimpleName());
                                fieldObj = getDaoMethod.invoke(greenDaoBase.session);
                                IOC.put(simpleName,fieldObj);
                            }
                        } else {
                            fieldObj = fieldClass.newInstance();
                            put(fieldClass.getSimpleName(),fieldObj);
                        }
                        saveFieldInstance(fieldObj,fieldClass,greenDaoBase);
                    }
                    field.setAccessible(true);
                    field.set(obj, fieldObj);
                }
            }
        } catch (IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }

    }

    public static Object get(String key) {
        return ioc.get(key);
    }

    public static Object put(String key, Object obj) {
        ioc.put(key, obj);
        return ioc.get(key);
    }

    /**
     * 从IOC容器获取并克隆出来
     *
     * @param key
     * @return
     */
    public static Object clone(String key) {
        Object obj = ioc.get(key);
        if (obj == null) {
            throw new NullPointerException("this key not find");
        }

        Class clazzs = obj.getClass().getSuperclass();
        if (clazzs == AbstractCloneable.class) {
            AbstractCloneable cloneable = (AbstractCloneable) ioc.get(key);
            return cloneable.clone();
        }
        return null;
    }
}
