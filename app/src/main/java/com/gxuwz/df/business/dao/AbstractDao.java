package com.gxuwz.df.business.dao;

import com.gxuwz.df.ui.fragment.BaseFragment;

import java.util.List;

public abstract class AbstractDao<T> {

    public List<T> findAll(BaseFragment view){
        return null;
    }

    public T add(BaseFragment view,T t){
        System.out.println("增加了");
        return t;
    }

    public boolean update(BaseFragment view,T t){
        System.out.println("更新了");
        return true;
    }

    public T findById(BaseFragment view,int id){
        System.out.println("查询了");
        return null;
    }

    public boolean delete(BaseFragment view,int id){
        System.out.println("删除了");
        return true;
    }

}
