package com.gxuwz.df.business.utils.comm;

import androidx.annotation.NonNull;

public abstract class AbstractCloneable implements Cloneable{

    @NonNull
    @Override
    public Object clone(){
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }
}
