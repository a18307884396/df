package com.gxuwz.df.business.model.vo;

import android.graphics.Bitmap;

import com.gxuwz.df.business.model.entity.User;

public class UserViewVo extends User {

    protected Bitmap userPicture;

    public UserViewVo() {
        super();
    }

    public Bitmap getUserPicture() {
        return userPicture;
    }

    public void setUserPicture(Bitmap userPicture) {
        this.userPicture = userPicture;
    }
}
