package com.gxuwz.df.business.model.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class Song{

    private String songName;
    private String songImg;

    public Song() {
    }

    @Generated(hash = 668066390)
    public Song(String songName, String songImg) {
        this.songName = songName;
        this.songImg = songImg;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public String getSongImg() {
        return songImg;
    }

    public void setSongImg(String songImg) {
        this.songImg = songImg;
    }
}
