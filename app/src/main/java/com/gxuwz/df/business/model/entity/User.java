package com.gxuwz.df.business.model.entity;

import com.gxuwz.df.ioc.DfBean;

@DfBean
public class User {
    protected String userId;
    protected String userName;
    private String pictureUrl;
    protected Integer grade;
    protected Boolean isMember;

    public User() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Boolean getMember() {
        return isMember;
    }

    public void setMember(Boolean member) {
        isMember = member;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", pictureUrl='" + pictureUrl + '\'' +
                ", grade=" + grade +
                ", isMember=" + isMember +
                '}';
    }
}
