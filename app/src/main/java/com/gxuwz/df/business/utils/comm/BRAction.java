package com.gxuwz.df.business.utils.comm;

public enum BRAction {

    PLAY_SONG_ACTION("play_song_action","播放命令"),
    STOP_SONG_ACTION("stop_song_action","停止命令");

    private String action;
    private String remark;

    BRAction(String action,String remark){
        this.action = action;
        this.remark = remark;
    }

    public String getAction(){
        return this.action;
    }
}
