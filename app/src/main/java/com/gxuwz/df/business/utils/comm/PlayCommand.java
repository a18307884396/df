package com.gxuwz.df.business.utils.comm;

import android.content.Intent;

import com.gxuwz.df.business.model.entity.Song;
import com.gxuwz.df.ioc.IOC;

public class PlayCommand extends BaseCommand{
    public Intent getPlayCommand(Song song){
        intent.setAction(BRAction.PLAY_SONG_ACTION.getAction());
        IOC.put("song",song);
        return intent;
    }

    public Intent getStopCommand(Song song){
        intent.setAction(BRAction.STOP_SONG_ACTION.getAction());
        return intent;
    }
}
