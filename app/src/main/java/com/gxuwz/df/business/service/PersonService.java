package com.gxuwz.df.business.service;

import com.gxuwz.df.business.greendao.PersonInforDao;
import com.gxuwz.df.business.model.entity.PersonInfor;
import com.gxuwz.df.ioc.Auto;
import com.gxuwz.df.ioc.DfBean;

import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;

@DfBean
public class PersonService {

    @Auto
    PersonInforDao personInforDao;

    public long add(PersonInfor personInfor){
        return personInforDao.insert(personInfor);
    }

    public List<PersonInfor> findAll(){
        return personInforDao
                .queryBuilder()
                .build()
                .list();
    }

    public void test(){

    }
}
