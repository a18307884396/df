package com.gxuwz.df.business.dao;

import com.gxuwz.df.R;
import com.gxuwz.df.business.model.entity.Songlist;
import com.gxuwz.df.ioc.DfBean;
import com.gxuwz.df.ioc.IOC;
import com.gxuwz.df.ui.fragment.BaseFragment;

import java.util.ArrayList;
import java.util.List;

@DfBean
public class SonglistDao extends AbstractDao<Songlist>{

    public void findUserSl(BaseFragment view){
        List<Songlist> songlists = new ArrayList<>();
        for(int i = 0;i < 2;i++){
            Songlist songlist = (Songlist) IOC.clone("Songlist");
            songlist.setSonglistName("周杰伦"+i);
            songlist.setSongNumber(5*(i+1));
            songlist.setSonglistUrl(R.drawable.img_my+"");
            songlists.add(songlist);
        }
        view.fieldInvoke("views","titleListView","setSonglists",songlists);
    }

}
