package com.gxuwz.df.business.service;

import android.graphics.drawable.BitmapDrawable;

import com.gxuwz.df.R;
import com.gxuwz.df.business.dao.UserDao;
import com.gxuwz.df.business.model.entity.Songlist;
import com.gxuwz.df.business.model.entity.User;
import com.gxuwz.df.business.model.vo.UserFunVo;
import com.gxuwz.df.business.model.vo.UserViewVo;
import com.gxuwz.df.ioc.Auto;
import com.gxuwz.df.ioc.DfBean;
import com.gxuwz.df.ioc.IOC;
import com.gxuwz.df.ui.fragment.BaseFragment;

import java.util.ArrayList;
import java.util.List;

@DfBean
public class UserService {

    @Auto
    UserDao userDao;

    public void findById(BaseFragment view){
        userDao.findById(view,0);
    }

    public void findFun(BaseFragment view){
        userDao.findFun(view);
    }
}
