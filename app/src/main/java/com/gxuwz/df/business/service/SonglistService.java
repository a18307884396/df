package com.gxuwz.df.business.service;

import com.gxuwz.df.business.dao.SonglistDao;
import com.gxuwz.df.ioc.Auto;
import com.gxuwz.df.ioc.DfBean;
import com.gxuwz.df.ui.fragment.BaseFragment;

@DfBean
public class SonglistService {

    @Auto
    SonglistDao songlistDao;

    public void findUserSl(BaseFragment view){
        songlistDao.findUserSl(view);
    }

}
