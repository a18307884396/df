package com.gxuwz.df.business.model.entity;

import com.gxuwz.df.business.utils.comm.AbstractCloneable;
import com.gxuwz.df.ioc.DfBean;

/**
 * 歌单实体类
 */
@DfBean
public class Songlist extends AbstractCloneable {
    protected String songlistName;
    protected Integer songNumber;
    private String songlistUrl;

    public String getSonglistName() {
        return songlistName;
    }

    public void setSonglistName(String songlistName) {
        this.songlistName = songlistName;
    }

    public Integer getSongNumber() {
        return songNumber;
    }

    public void setSongNumber(Integer songNumber) {
        this.songNumber = songNumber;
    }

    public String getSonglistUrl() {
        return songlistUrl;
    }

    public void setSonglistUrl(String songlistUrl) {
        this.songlistUrl = songlistUrl;
    }
}
