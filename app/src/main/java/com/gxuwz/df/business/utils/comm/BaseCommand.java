package com.gxuwz.df.business.utils.comm;

import android.content.Intent;

import com.gxuwz.df.ioc.IOC;
import com.gxuwz.df.business.model.entity.Song;

public abstract class BaseCommand {

    protected Intent intent = (Intent) IOC.put("intent",new Intent());

    public Intent getPlayCommand(Song song){
        intent.setAction(BRAction.PLAY_SONG_ACTION.getAction());
        IOC.put("song",song);
        return intent;
    }

    public Intent getStopCommand(Song song){
        intent.setAction(BRAction.STOP_SONG_ACTION.getAction());
        return intent;
    }

}
