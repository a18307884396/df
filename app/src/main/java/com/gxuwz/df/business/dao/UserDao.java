package com.gxuwz.df.business.dao;

import android.graphics.drawable.BitmapDrawable;

import com.gxuwz.df.R;
import com.gxuwz.df.business.model.entity.User;
import com.gxuwz.df.business.model.vo.UserFunVo;
import com.gxuwz.df.business.model.vo.UserViewVo;
import com.gxuwz.df.ioc.Auto;
import com.gxuwz.df.ioc.DfBean;
import com.gxuwz.df.ioc.IOC;
import com.gxuwz.df.ui.fragment.BaseFragment;
import com.gxuwz.df.ui.util.retrofit.RetrofitUtil;

import java.util.ArrayList;
import java.util.List;

@DfBean
public class UserDao extends AbstractDao<User>{

    @Auto
    RetrofitUtil retrofitUtil;

    public void findFun(BaseFragment view){
        List<UserFunVo> userFunVos = new ArrayList<>();
        BitmapDrawable bitmapDrawable = null;
        for(int i = 0;i < 4;i++){
            UserFunVo userFunVo = (UserFunVo) IOC.clone("UserFunVo");
            bitmapDrawable = (BitmapDrawable) view.getContext().getDrawable(R.drawable.icon_music);
            userFunVo.setFunIcon(bitmapDrawable.getBitmap());
            userFunVo.setFunName("本地音乐");
            userFunVos.add(userFunVo);
        }
        view.fieldInvoke("views","userFunView","setUserFunVos",userFunVos);
    }

    @Override
    public User findById(BaseFragment view,int id) {
        UserViewVo userViewVo = new UserViewVo();
        BitmapDrawable bitmapDrawable = (BitmapDrawable) view.getContext().getDrawable(R.drawable.img_my);
        userViewVo.setUserName("马丁路德·金");
        userViewVo.setGrade(5);
        userViewVo.setUserPicture(bitmapDrawable.getBitmap());
        view.fieldInvoke("views","userSimpleMessView",
                "setUserViewVo",userViewVo);
        return null;
    }
}
