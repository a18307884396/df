package com.gxuwz.df.business.model.vo;

import android.graphics.Bitmap;

import androidx.annotation.NonNull;

import com.gxuwz.df.business.utils.comm.AbstractCloneable;
import com.gxuwz.df.ioc.DfBean;

/**
 * 用户功能Vo
 */
@DfBean
public class UserFunVo extends AbstractCloneable {
    private Bitmap funIcon;
    private String funName;
    private Integer url;

    public UserFunVo() {
    }

    public Bitmap getFunIcon() {
        return funIcon;
    }

    public void setFunIcon(Bitmap funIcon) {
        this.funIcon = funIcon;
    }

    public String getFunName() {
        return funName;
    }

    public void setFunName(String funName) {
        this.funName = funName;
    }

    public Integer getUrl() {
        return url;
    }

    public void setUrl(Integer url) {
        this.url = url;
    }

}
